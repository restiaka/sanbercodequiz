import React, { useContext } from 'react';
import { StyleSheet, View, Text, FlatList } from 'react-native';
import { RootContext } from './Soal2'

const PositionList = () => {
	const state = useContext(RootContext)

	const renderItem = ({item,index}) => {
		return (
			<View style={styles.list}>
				<View>
					<Text>{item.name}</Text>
					<Text>{item.position}</Text>
				</View>
			</View>
		)
	}

	return (
		<View style={styles.container}>
			<View style={styles.listContainer}>
				<FlatList
					data={state.name}
					renderItem={renderItem}
					keyExtractor={(item,index)=>index.toString()}
					extraData={state.refresh}
				/>
			</View>
		</View>
	)

}

const styles = StyleSheet.create({
	container: {
		flex: 1
	},
	row: {
		flex: 1,
		flexDirection: 'row'
	},
	listContainer: {
		flex: 1,
		paddingHorizontal: 10,
		marginTop: 50
	},
	list: {
		padding: 20,
	    paddingRight: 100,
	    borderWidth: 5,
	    borderRadius: 5,
	    borderColor: '#ededed',
	    marginBottom: 10
	}
});

export default PositionList;