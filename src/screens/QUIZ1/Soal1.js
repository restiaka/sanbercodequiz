import React, { useState, useEffect } from 'react'
import { Text, View } from 'react-native'

const FComponent = () => {
	const [ name, setName ] = useState('Jhon Doe')

	useEffect(()=>{
		setTimeout(()=>{
			setName('Asep')
		},3000)
	}, [name])

	return (
		<View>
			<Text>
			  {name}
			</Text>
		</View>
	)
}

export default FComponent;